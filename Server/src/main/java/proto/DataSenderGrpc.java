package proto;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: Server.proto")
public final class DataSenderGrpc {

  private DataSenderGrpc() {}

  public static final String SERVICE_NAME = "DataSender";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<proto.Server.Date,
      proto.Server.ReplyMessage> getGetZodiacMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetZodiac",
      requestType = proto.Server.Date.class,
      responseType = proto.Server.ReplyMessage.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<proto.Server.Date,
      proto.Server.ReplyMessage> getGetZodiacMethod() {
    io.grpc.MethodDescriptor<proto.Server.Date, proto.Server.ReplyMessage> getGetZodiacMethod;
    if ((getGetZodiacMethod = DataSenderGrpc.getGetZodiacMethod) == null) {
      synchronized (DataSenderGrpc.class) {
        if ((getGetZodiacMethod = DataSenderGrpc.getGetZodiacMethod) == null) {
          DataSenderGrpc.getGetZodiacMethod = getGetZodiacMethod = 
              io.grpc.MethodDescriptor.<proto.Server.Date, proto.Server.ReplyMessage>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "DataSender", "GetZodiac"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.Server.Date.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.Server.ReplyMessage.getDefaultInstance()))
                  .setSchemaDescriptor(new DataSenderMethodDescriptorSupplier("GetZodiac"))
                  .build();
          }
        }
     }
     return getGetZodiacMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static DataSenderStub newStub(io.grpc.Channel channel) {
    return new DataSenderStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static DataSenderBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new DataSenderBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static DataSenderFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new DataSenderFutureStub(channel);
  }

  /**
   */
  public static abstract class DataSenderImplBase implements io.grpc.BindableService {

    /**
     */
    public void getZodiac(proto.Server.Date request,
        io.grpc.stub.StreamObserver<proto.Server.ReplyMessage> responseObserver) {
      asyncUnimplementedUnaryCall(getGetZodiacMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetZodiacMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                proto.Server.Date,
                proto.Server.ReplyMessage>(
                  this, METHODID_GET_ZODIAC)))
          .build();
    }
  }

  /**
   */
  public static final class DataSenderStub extends io.grpc.stub.AbstractStub<DataSenderStub> {
    private DataSenderStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DataSenderStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DataSenderStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DataSenderStub(channel, callOptions);
    }

    /**
     */
    public void getZodiac(proto.Server.Date request,
        io.grpc.stub.StreamObserver<proto.Server.ReplyMessage> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetZodiacMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class DataSenderBlockingStub extends io.grpc.stub.AbstractStub<DataSenderBlockingStub> {
    private DataSenderBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DataSenderBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DataSenderBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DataSenderBlockingStub(channel, callOptions);
    }

    /**
     */
    public proto.Server.ReplyMessage getZodiac(proto.Server.Date request) {
      return blockingUnaryCall(
          getChannel(), getGetZodiacMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class DataSenderFutureStub extends io.grpc.stub.AbstractStub<DataSenderFutureStub> {
    private DataSenderFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DataSenderFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DataSenderFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DataSenderFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<proto.Server.ReplyMessage> getZodiac(
        proto.Server.Date request) {
      return futureUnaryCall(
          getChannel().newCall(getGetZodiacMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_ZODIAC = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final DataSenderImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(DataSenderImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_ZODIAC:
          serviceImpl.getZodiac((proto.Server.Date) request,
              (io.grpc.stub.StreamObserver<proto.Server.ReplyMessage>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class DataSenderBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    DataSenderBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return proto.Server.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("DataSender");
    }
  }

  private static final class DataSenderFileDescriptorSupplier
      extends DataSenderBaseDescriptorSupplier {
    DataSenderFileDescriptorSupplier() {}
  }

  private static final class DataSenderMethodDescriptorSupplier
      extends DataSenderBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    DataSenderMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (DataSenderGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new DataSenderFileDescriptorSupplier())
              .addMethod(getGetZodiacMethod())
              .build();
        }
      }
    }
    return result;
  }
}
