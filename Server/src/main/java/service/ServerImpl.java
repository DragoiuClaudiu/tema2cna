package service;

import io.grpc.stub.StreamObserver;
import proto.DataSenderGrpc;
import proto.Server;
import zodiacpack.Zodiac;
import zodiacpack.ZodiacMethods;

import java.util.ArrayList;
import java.util.List;

public class ServerImpl extends DataSenderGrpc.DataSenderImplBase {
    private List<Zodiac> zodiacList;
    private ZodiacMethods zodiacMethods;

    @Override
    public void getZodiac(Server.Date request, StreamObserver<Server.ReplyMessage> responseObserver) {
        zodiacList = new ArrayList<>();
        zodiacMethods = new ZodiacMethods();

        zodiacMethods.getZodiacTable(zodiacList,"zodiac.txt");
        Server.ReplyMessage replyMessage;

        replyMessage= Server.ReplyMessage.newBuilder().setZodiac(zodiacMethods.getZodiacForDate(request.getDate(),zodiacList)).build();

        responseObserver.onNext(replyMessage);
        responseObserver.onCompleted();
    }
}
