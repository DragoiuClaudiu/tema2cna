package zodiacpack;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

public class ZodiacMethods {

    public void getZodiacTable(List<Zodiac> zodiacList,String fileName){
        try {
            File myObj = new File(fileName);
            Scanner scanner = new Scanner(myObj);
            while (scanner.hasNextLine()) {
                Zodiac zodiac;
                String data = scanner.nextLine();
                String[] strings=data.split("-");
                zodiac=new Zodiac(strings[0],strings[1],strings[2]);
                zodiacList.add(zodiac);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
        }
    }

    public String getZodiacForDate(String zodiacDate, List<Zodiac> zodiacList){

        int zodiacDay=Integer.parseInt(zodiacDate.split("-")[1]);
        int zodiacMonth=Integer.parseInt(zodiacDate.split("-")[0]);

        for(int index=0;index<zodiacList.size();index++) {

            int zodiacStartDay = Integer.parseInt(zodiacList.get(index).getStartDate().split("-")[1]);
            int zodiacStartMonth = Integer.parseInt(zodiacList.get(index).getStartDate().split("-")[0]);
            int zodiacEndDay = Integer.parseInt(zodiacList.get(index).getEndDate().split("-")[1]);
            int zodiacEndMonth = Integer.parseInt(zodiacList.get(index).getEndDate().split("-")[0]);

            if(zodiacMonth>zodiacStartMonth && zodiacMonth<zodiacEndMonth)
            {
                return zodiacList.get(index).getName();
            }else if(zodiacMonth==zodiacStartMonth && zodiacDay>=zodiacStartDay)
            {
                return zodiacList.get(index).getName();
            } else if(zodiacMonth==zodiacEndMonth && zodiacDay<=zodiacEndDay){
                return zodiacList.get(index).getName();
            }
        }
        return "";
    }

}

