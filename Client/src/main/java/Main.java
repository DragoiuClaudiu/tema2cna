import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import proto.Clients;
import proto.DataSenderGrpc;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8999).usePlaintext().build();
        DataSenderGrpc.DataSenderBlockingStub clientsStub = DataSenderGrpc.newBlockingStub(channel);

        String zodiacDate;
        Boolean zodiacIsValid;
        Scanner scanner=new Scanner(System.in);
        DateValidator validator = new DateValidator("MM/dd/yyyy");

        System.out.println("Insert a date to check the zodiac sign:");
        zodiacDate=scanner.nextLine();

        zodiacIsValid=validator.isValid(zodiacDate);

        while (zodiacIsValid==false)
        {
            System.out.println("Input failed!");
            System.out.println("Insert a valid date:");
            zodiacDate=scanner.nextLine();
            zodiacIsValid=validator.isValid(zodiacDate);
        }
        System.out.println("Input succesfully");

        Clients.ReplyMessage replyMessage=clientsStub.getZodiac(Clients.Date.newBuilder().setDate(zodiacDate).build());
        System.out.println(replyMessage.getZodiac());
        channel.shutdown();


        //clientsStub.getNameCNP(Clients.theNameCNP.newBuilder().setName(name).setCNP(CNP).build());



        channel.shutdown();
    }
}
